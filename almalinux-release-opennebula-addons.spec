Name:           almalinux-release-opennebula-addons
Version:        1
Release:        1%{?dist}
Summary:        OpenNebula addons for AlmaLinux

License:        MIT
URL:            https://wiki.almalinux.org/sigs/Cloud
Source0:        almalinux-opennebula-addons.repo
Requires:       almalinux-release
BuildArch:      noarch

%description
OpenNebula addons packages as delivered via the AlmaLinux Cloud SIG.

%prep

%build

%install
mkdir -p %{buildroot}%{_sysconfdir}/yum.repos.d
install -Dpm0644 -t %{buildroot}%{_sysconfdir}/yum.repos.d %{S:0}

%files
%config(noreplace) %{_sysconfdir}/yum.repos.d/almalinux-opennebula-addons.repo

%changelog
* Tue Sep 07 2021 Andrew Lukoshko <alukoshko@almalinux.org> - 1-1
- Initial package


